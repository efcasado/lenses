PROJECT = lenses

DEPS = cowboy jsx
dep_cowboy = git https://github.com/ninenines/cowboy 1.0.1
dep_jsx = git https://github.com/talentdeficit/jsx v2.3.0

include erlang.mk

clean::
	@find . -type f -name "*~" -exec rm {} \;
