Lenses
======

Lenses is an Erlang/OTP application that periodically takes snapshots of
the Erlang environment it is running on. The information gathered in the
snapshots is visualised in a simple yet convenient web dashboard.