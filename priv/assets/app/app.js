(function(){
    var app = angular.module('stats', []);

    app.controller('StatsController', ['$scope', '$http', function($scope, $http){
        // Head of the processes table
        $scope.head =
            [
                // object attribute, column header
                ['pid', 'PID'],
                ['registered_name', 'Registered Name'],
                ['message_queue_length', 'Message Queue (Lenght)'],
                ['memory', 'Memory'],
                ['reductions', 'Reductions'],
                ['first_seen', 'First Seen'],
            ];

        // Processes displayed in the process table
        $scope.processes = [];

        // Schedulers utilisation
        $scope.schedulers = [];

        // Memory utilisation
        $scope.memory =
            {
                labels: ["", "", "", "", "", "", "", "", "", ""],
                datasets: [
                    // total memory
                    {
                        strokeColor: "#F7464A",
                        label: "Total",
                        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    },
                    // processes memory
                    {
                        strokeColor: "#46BFBD",
                        label: "Processes",
                        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    },
                    // system memory
                    {
                        strokeColor: "#FDB45C",
                        label: "System",
                        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    }
                ]
            };


        var ctx1 = document.getElementById("schdsUtilChart").getContext("2d");
        var schdsUtilChart =
            new Chart(ctx1).Doughnut(
                $scope.schedulers,
                {
                    showTooltips: false,
                    animation: false,
                    legendTemplate : "<ul style=\"list-style:none; padding-left: 0px\" class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>; vertical-align: middle; font-size: 10px; padding-right: 12px; margin-right: 5px\"></span>Sch. #<%=segments[i].label%> (<%=segments[i].value%>%)</li><%}%></ul>"
                });

        var ctx2 = document.getElementById("memUtilChart").getContext("2d");
        var memUtilChart =
            new Chart(ctx2).Line(
                $scope.memory,
                {
                    animation: false,
                    showTooltips: false,
                    bezierCurve: false,
                    pointDot: false,
                    scaleFontSize: 8,
                    datasetFill: false,
                    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\" style=\"display: inline; padding-left: 0px\"><% for (var i=0; i<datasets.length; i++){%><li style=\"display: inline-block; margin-right: 5px\"><span style=\"background-color:<%=datasets[i].strokeColor%>; vertical-align: middle; font-size: 10px; padding-left: 12px; margin-left: 5px; margin-right: 3px\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

                });

        // How the processes table is sorted
        $scope.sort = {
            column: 'reductions',
            descending: false
        };

        $scope.sortTable = function(column){
            if($scope.sort.column == column) {
                $scope.sort.descending = !$scope.sort.descending;
            };
            $scope.sort.column = column;
        };

        $scope.prekill = function(pid) {
            $scope.foo = pid;
        };

        $scope.kill = function(pid) {
            $http.delete("/procs/" + pid).
                success(function() {
                    $scope.processes =
                        $scope.processes.filter(function(v, i, ar) {
                            return v.pid != pid
                        });
                    new PNotify({
                        title: "Process Termination",
                        text: "Process " + pid + " successfully terminated.",
                        type: "success",
                        delay: 3000
                    });
                }).
                error(function() {
                    new PNotify({
                        title: "Process Termination",
                        text: "An error has occurred when attempting to kill process " + pid + " .",
                        type: "error",
                        delay: 3000
                    });
                });
        };

        formatSchdsInfo = function(data){
            return data.map(function(schd) {
                return {
                    value: parseFloat(schd.utilisation),
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                    label: schd.scheduler
                }
            });
        };

        // Function used to update the schedulers utilisation chart
        updateSchdsUtilChart = function(data) {
            var colours =
                [
                    "#F7464A",
                    "#46BFBD",
                    "#FDB45C",
                    "#751E51",
                    "#1B6AA5",
                    "#CCCC00",
                    "#D78CC4",
                    "#1C6F7B"
                ];

            var formattedData = data.map(function(schdInfo, i) {
                var formattedSchdInfo =
                    {
                        value: parseFloat(schdInfo.utilisation),
                        color: colours[i % colours.length],
                        label: schdInfo.scheduler
                    };


                return formattedSchdInfo;

            });

            // Clear old data
            data.forEach(function(_x) { schdsUtilChart.removeData(); });
            // Add new data
            formattedData.forEach(function(schdInfo){
                schdsUtilChart.addData(schdInfo);
            });
            // Update chart
            schdsUtilChart.update();
            // Update legend
            $("#schdsUtilLegend").html(schdsUtilChart.generateLegend());
        };

        //var tick = 0;
        updateMemUtilChart = function(data) {
            //if (tick < 10) tick = tick + 1;
            //else memUtilChart.removeData();
            memUtilChart.removeData();
            memUtilChart.addData([data.total, data.processes, data.system], "");
            memUtilChart.update();
            $("#memUtilLegend").html(memUtilChart.generateLegend());
        };

        // Populate processes table with data from the websocket connection
        new WebSocket("ws://localhost:8080/ws/stats")
            .addEventListener("message", function(event){
                $scope.$apply(function(){
                    var data = JSON.parse(event.data);
                    // Update schedulers info
                    updateSchdsUtilChart(data.schedulers);
                    // Update memory info
                    updateMemUtilChart(data.memory);
                    // Update processes info
                    $scope.processes = data.processes;
                });
            });

    }]);

    app.controller('SnapshotIntervalController', ['$scope', '$http', function($scope, $http){

        $scope.interval = 1; // in seconds

        $scope.clearForm = function() {
            $("#intervalGroup").removeClass("has-error");
            $scope.interval = 1;
        };

        $scope.setInterval = function(interval) {
            // send POST request to the server and update the form if necessary (error)
            var req = {
                method: 'POST',
                url: "/settings/snapshot-interval?interval=" + interval,
                headers: {'content-type': 'application/x-www-form-urlencoded'},
                data: ""
            };

            $http(req).success(function(){
                new PNotify({
                    title: "Snapshot Interval",
                    text: "Snapshot Interval has been successfuly set to " + interval + " seconds.",
                    type: "success",
                    delay: 3000
                });
                $("#snapshotIntervalForm").modal('hide');
                $scope.clearForm();
            }).error(function(){
                $("#intervalGroup").addClass("has-error");
            });
        };

    }]);
})()
