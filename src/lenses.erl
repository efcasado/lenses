-module(lenses).

-export([main/1]).

main(_) ->
    process_flag(trap_exit, true),

    {ok, _} = application:ensure_all_started(?MODULE),

    AppSup = whereis(lenses_sup),
    link(AppSup),

    loop().

loop() ->
    receive
        Msg ->
            io:format("Message Received: ~p~n", [Msg]),
            halt(1)
    end.
