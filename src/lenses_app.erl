%%%========================================================================
%%% File: lenses_app.erl
%%%
%%% TBD
%%%
%%% Author: Enrique Fernandez <enrique.fernandez@erlang-solutions.com>
%%%========================================================================
-module(lenses_app).

-behaviour(application).

-export([start/2, stop/1]).

%% ========================================================================
%%  API
%% ========================================================================

start(_Type, _Args) ->
    %% Start webserver application
    {ok, _Pid1} = start_webserver(),

    %% Start lenses' superivisor
    {ok, _Pid2} = lenses_sup:start_link().


stop(_State) ->
	ok.


%% ========================================================================
%%  Local functions
%% ========================================================================

start_webserver() ->
    Routes = read_routes(),
    Dispatch = cowboy_router:compile([{'_', Routes}]),

    cowboy:start_http(http,
                      100,
                      [
                       {port, 8080}
                      ],
                      [
                       {env, [{dispatch, Dispatch}]}
                      ]).

read_routes() ->
    RoutesFromFile =
        [
         {"/ws/stats", lenses_ws_handler_stats, []},
         {"/procs/:pid", lenses_rest_procs, []},
         {"/settings/snapshot-interval", lenses_rest_settings_interval, []}
        ],
    StaticFiles = {"/[...]", cowboy_static,
                   {priv_dir, lenses, "assets",
                    [
                     {mimetypes, cow_mimetypes, all},
                     {dir_handler, directory_handler}
                    ]}},
    lists:append(RoutesFromFile, [StaticFiles]).
