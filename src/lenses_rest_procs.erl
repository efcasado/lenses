%%%========================================================================
%%% File: lenses_rest_procs.erl
%%%
%%% Cowboy's REST handler used to set the snapshot interval from the
%%% web application.
%%%
%%% Author: Enrique Fernandez <enrique.fernandez@erlang-solutions.com>
%%%========================================================================
-module(lenses_rest_procs).

%% cowboy's REST handler callbacks
-export(
   [
    init/3,
    allowed_methods/2,
    delete_resource/2
   ]).



%% ========================================================================
%%  Local functions
%% ========================================================================

%%-------------------------------------------------------------------------
%% @doc
%% Convert the provided binary PID into a raw PID and kill the Erlang
%% process referenced by it.
%% @end
%%------------------------------------------------------------------------
kill(Pid) ->
    exit(list_to_pid(binary_to_list(Pid)), kill).

%% ========================================================================
%%  Cowboy's REST handler callbacks
%% ========================================================================

init(_Type, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
    {[<<"DELETE">>], Req, State}.

delete_resource(Req, State) ->
    {Pid, _} = cowboy_req:binding(pid, Req),
    try
        kill(Pid),
        {true, Req, State}
    catch
        _:_ ->
            {false, Req, State}
    end.
