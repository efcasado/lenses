%%%========================================================================
%%% File: lenses_rest_settings_interval.erl
%%%
%%% Cowboy's REST handler used to set the snapshot interval from the
%%% web application.
%%%
%%% Author: Enrique Fernandez <enrique.fernandez@erlang-solutions.com>
%%%========================================================================
-module(lenses_rest_settings_interval).

%% cowboy's REST handler callbacks
-export(
   [
    init/3,
    allowed_methods/2,
    content_types_accepted/2
   ]).

%% local functions (callable from cowboy app)
-export(
   [
    set_snapshot_interval/2
   ]).


%% ========================================================================
%%  Local functions
%% ========================================================================

%%-------------------------------------------------------------------------
%% @doc
%% TBD
%% @end
%%-------------------------------------------------------------------------
set_snapshot_interval(Req, State) ->
    {RawInterval, _} = cowboy_req:qs_val(<<"interval">>, Req),
    Interval = binary_to_integer(RawInterval),
    case valid_interval(Interval) of
        true ->
            ok = pusher:set_refresh_interval(Interval * 1000),
            {true, Req, State};
        false ->
            {false, Req, State}
    end.

%%-------------------------------------------------------------------------
%% @doc
%% Check if the user-entered snapshot interval is valid.
%% @end
%%-------------------------------------------------------------------------
valid_interval(Interval) ->
    Interval >= 1 andalso Interval =< 60.

%% ========================================================================
%%  Cowboy's REST handler callbacks
%% ========================================================================

init(_Type, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
    {[<<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
    {
      [
       {{<<"application">>, <<"x-www-form-urlencoded">>, []}, set_snapshot_interval}
      ],
      Req, State }.
