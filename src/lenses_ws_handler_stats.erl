%%%========================================================================
%%% File: lenses_ws_handler_stats.erl
%%%
%%% Author: Enrique Fernandez <enrique.fernandez@erlang-solutions.com>
%%%========================================================================
-module(lenses_ws_handler_stats).

-behaviour(cowboy_websocket_handler).

%% Cowboy websocket-handler callbacks
-export(
   [
    init/3,
    websocket_init/3,
    websocket_handle/3,
    websocket_info/3,
    websocket_terminate/3
   ]).



%% ========================================================================
%%  Cowboy websocket-handler callbacks
%% ========================================================================

init(_Type, Req, Opts) ->
    {upgrade, protocol, cowboy_websocket, Req, Opts}.

websocket_init(_Type, Req, _Opts) ->
    ok = pusher:subscribe(),
    {ok, Req, undefined}.

websocket_handle(_Frame, Req, State) ->
    {ok, Req, State}.

websocket_info({stats, Stats}, Req, State) ->
    {reply, {text, jsx:encode(Stats)}, Req, State};
websocket_info(_Msg, Req, State) ->
    {ok, Req, State}.

websocket_terminate(_Reason, _Req, _State) ->
    pusher:unsubscribe(),
    ok.

%% ========================================================================
%%  Local functions
%% ========================================================================
