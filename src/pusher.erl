%%%========================================================================
%%% File: pusher.erl
%%%
%%% Process responsible for fetching statistics and pushing them to all
%%% subscribed processes.
%%%
%%% Author: Enrique Fernandez <enrique.fernandez@erlang-solutions.com>
%%%========================================================================
-module(pusher).

-behaviour(gen_server).

%% API
-export(
   [
    start_link/0,
    subscribe/0, subscribe/1,
    unsubscribe/0,
    set_refresh_interval/1
   ]).

%% gen_server callbacks
-export(
   [
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
   ]).


%% ========================================================================
%%  Macro definitions
%% ========================================================================

%% Name the pusher process is registered under.
-define(SERVER, ?MODULE).

%% Default refresh interval (in milliseconds).
-define(DEFAULT_REFRESH_INTERVAL, 1000).

-define(SUPPORTED_MEMORY_TYPES,
        [
         total,
         processes,
         system
        ]).


%% ========================================================================
%%  Record definitions
%% ========================================================================

-record(snapshot,
        {
          timestamp,
          procs = [],
          schds = [],
          mem = []
        }).

-record(state,
        {
          subscribers      = sets:new()  :: sets:set(pid()),
          refresh_interval = 1000        :: non_neg_integer(),
          snapshot         = #snapshot{} :: snapshot()
        }).


%% ========================================================================
%%  Type definitions
%% ========================================================================

-type snapshot()             :: #snapshot{}.
-type refresh_interval_opt() :: {'refresh_interval', non_neg_integer()}.
-type opt()                  :: refresh_interval_opt().


%% ========================================================================
%%  API
%% ========================================================================

%%-------------------------------------------------------------------------
%% @doc
%% Start the pusher process. Note that the pusher processes is (locally)
%% registered under the name "pusher".
%% @end
%%-------------------------------------------------------------------------
-spec start_link() -> {'ok', pid()}.
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%-------------------------------------------------------------------------
%% @doc
%% Subscribe the caller to stats information about the Erlang node where
%% this application is running on.
%% @end
%%-------------------------------------------------------------------------
-spec subscribe() -> 'ok'.
subscribe() ->
    Opts =
        [
         {refresh_interval, ?DEFAULT_REFRESH_INTERVAL}
        ],
    subscribe(Opts).

%%-------------------------------------------------------------------------
%% @doc
%% Subscribe the caller to stats information about the Erlang node where
%% this application is running on. The callee process is subscribed using
%% the provided options.
%% @end
%%-------------------------------------------------------------------------
-spec subscribe([opt()]) -> 'ok'.
subscribe(Opts) ->
    ok = gen_server:call(?SERVER, {subscribe, self(), Opts}).

%%-------------------------------------------------------------------------
%% @doc
%% Unsubscribe the caller process so that it does not receive more stats
%% updates.
%% @end
%%-------------------------------------------------------------------------
-spec unsubscribe() -> 'ok'.
unsubscribe() ->
    ok = gen_server:call(?SERVER, {unsubscribe, self()}).

%%-------------------------------------------------------------------------
%% @doc
%% Sets the refresh interval to the specified value.
%% @end
%%-------------------------------------------------------------------------
-spec set_refresh_interval(non_neg_integer()) -> 'ok'.
set_refresh_interval(RefreshInterval) ->
    ok = gen_server:call(?SERVER, {set_refresh_interval, RefreshInterval}).


%% ========================================================================
%%  gen_server callbacks
%% ========================================================================

init(_Args) ->
    erlang:system_flag(scheduler_wall_time, true),
    {ok, #state{}}.

handle_call({subscribe, Pid, Opts},
            _From,
            #state{ subscribers = Subscribers } = State) ->
    RefreshInterval = get_refresh_interval(Opts),
    case empty_subscribers(Subscribers) of
        true ->
            erlang:send_after(RefreshInterval, self(), flush);
        false ->
            ok
    end,
    UpdatedSubscribers = '_subscribe'(Pid, Subscribers),
    {reply, ok, State#state{ subscribers = UpdatedSubscribers,
                             refresh_interval = RefreshInterval }};
handle_call({unsubscribe, Pid},
            _From,
            #state{ subscribers = Subscribers } = State) ->
    UpdatedSubscribers = '_unsubscribe'(Pid, Subscribers),
    {reply, ok, State#state{ subscribers = UpdatedSubscribers }};
handle_call({set_refresh_interval, RefreshInterval}, _From, State) ->
    {reply, ok, State#state{ refresh_interval = RefreshInterval }};
handle_call(_Msg, _From, State) ->
    {noreply, State}.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(flush,
            #state
            {
              subscribers = Subscribers,
              refresh_interval = RefreshInterval,
              snapshot = OldSnapshot
            } = State) ->

    Snapshot =
        case empty_subscribers(Subscribers) of
            true ->
                ok;
            false ->
                NewSnapshot = take_snapshot(),
                UpdatedSnapshot = update_snapshot(OldSnapshot, NewSnapshot),
                push_snapshot(Subscribers, UpdatedSnapshot),
                erlang:send_after(RefreshInterval, self(), flush),
                UpdatedSnapshot
        end,
    {noreply, State#state{ snapshot = Snapshot }};
handle_info(_Msg, State) ->
    {noreply, State}.

terminate(_Reason, _State) ->
    erlang:system_flag(scheduler_wall_time, false),
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ========================================================================
%%  Local functions
%% ========================================================================

get_refresh_interval(Opts) ->
    proplists:get_value(refresh_interval, Opts, ?DEFAULT_REFRESH_INTERVAL).

'_subscribe'(Pid, Subscribers) ->
    sets:add_element(Pid, Subscribers).

'_unsubscribe'(Pid, Subscribers) ->
    sets:del_element(Pid, Subscribers).

empty_subscribers(Subscribers) ->
    sets:size(Subscribers) == 0.

push_snapshot(Subscribers, Snapshot) ->
    FmtdSnapshot = format_snapshot(Snapshot),
    sets:fold(fun(Pid, _Acc) -> push(Pid, FmtdSnapshot) end, ok, Subscribers).

take_snapshot() ->
    T = timestamp(),
    #snapshot{ timestamp = T,
               schds = schds_info(),
               procs = procs_info(T),
               mem = memory_info() }.

timestamp() ->
    erlang:universaltime().

update_snapshot(
  #snapshot{ schds = OldSchds, procs = OldProcs, mem = OldMem } = _OldSnapshot,
  #snapshot{ schds = NewSchds, procs = NewProcs, mem = NewMem } = _NewSnapshot) ->
    #snapshot{ schds = update_schedulers(OldSchds, NewSchds),
               procs = update_processes(OldProcs, NewProcs),
               mem = update_memory(OldMem, NewMem) }.

update_schedulers(OldSchds, NewSchds) ->
    '_update_schedulers'(OldSchds, NewSchds, []).

'_update_schedulers'([], NewSchds, []) ->
    %% First sample
    [ {SchID, AT, TT, 0, 0} || {SchID, AT, TT} <- NewSchds ];
    %% NewSchds;
'_update_schedulers'([], [], Schds) ->
    lists:reverse(Schds);
'_update_schedulers'(
  [{SchdID, AT1, TT1, _, _}| OSs] = _OldSchds,
  [{SchdID, AT2, TT2}| NSs] = _NewSchds,
  Schds) ->
    '_update_schedulers'(OSs, NSs, [{SchdID, AT2, TT2, AT1, TT1}| Schds]).


update_processes(OldProcs, NewProcs) ->
    '_update_processes'(OldProcs, NewProcs, []).

'_update_processes'([] = _OldProcs, NewProcs, Procs) ->
    lists:append(lists:reverse(Procs), NewProcs);
'_update_processes'(_OldProcs, [] = _NewProcs, Procs) ->
    lists:reverse(Procs);
'_update_processes'(
  [{Pid, _RN, _MQL, _HS, _R, T}| OPs],
  [{Pid, RN, MQL, HS, R, _T}| NPs],
  Procs) ->
    %% Take the latest information of the process, but replace its
    %% 'first_seen' field by its old value
    '_update_processes'(OPs, NPs, [{Pid, RN, MQL, HS, R, T}| Procs]);
'_update_processes'(
  [{Pid1, _, _, _, _, _}| OPs],
  [{Pid2, _, _, _, _, _}| _NPs] = NewProcs,
  Procs)
  when Pid1 < Pid2 ->
    %% Pid1 has been terminated
    '_update_processes'(OPs, NewProcs, Procs);
'_update_processes'(
  [{Pid1, _, _, _, _, _}| _OPs] = OldProcs,
  [P = {Pid2, _, _, _, _, _}| NPs],
  Procs)
  when Pid1 > Pid2 ->
    %% Pid2 is a new process
    '_update_processes'(OldProcs, NPs, [P| Procs]).

update_memory(_OldMem, NewMem) ->
    NewMem.


memory_info() ->
    [ MemInfo || {Type, _Size} = MemInfo <- erlang:memory(),
                 lists:member(Type, ?SUPPORTED_MEMORY_TYPES)].

schds_info() ->
    %% The list of scheduler information is unsorted and may appear in
    %% different order between calls.
    lists:sort(erlang:statistics(scheduler_wall_time)).

procs_info(T) ->
      [ {
          pid_to_binary(Pid),
          registered_name(Pid),
          message_queue_length(Pid),
          memory(Pid),
          reductions(Pid),
          T
        }
        || Pid <- processes() ].

pid_to_binary(Pid) ->
    list_to_binary(pid_to_list(Pid)).

registered_name(Pid) ->
    case process_info(Pid, registered_name) of
        [] ->
            <<"">>;
        {registered_name, RegName} ->
            RegName
    end.

message_queue_length(Pid) ->
    {message_queue_len, MsgQLen} = process_info(Pid, message_queue_len),
    MsgQLen.

memory(Pid) ->
    {memory, Memory} = process_info(Pid, memory),
    Memory.

reductions(Pid) ->
    {reductions, Reds} = process_info(Pid, reductions),
    Reds.

push(Pid, Stats) ->
    Pid ! {stats, Stats}.

format_snapshot(
  #snapshot{ schds = Schds,
             procs = Procs,
             mem = Mem }) ->
    [
     {schedulers, format_schedulers(Schds)},
     {processes, format_processes(Procs)},
     {memory, format_memory(Mem)}
    ].

format_timestamp(T) ->
    %% httpd_util module belongs to the inets applications
    list_to_binary(httpd_util:rfc1123_date(T)).

format_schedulers(Schds) ->
    [ [
       {scheduler, SchID},
       %% {utilisation, AT / TT * 100}
       {utilisation, float_to_binary((AT2 - AT1) / (TT2 - TT1) * 100, [{decimals, 2}])}
      ]
      || {SchID, AT2, TT2, AT1, TT1} <- Schds ].

format_processes(Procs) ->
    [ [
       {pid, Pid},
       {registered_name, RegName},
       {message_queue_length, MsgQLen},
       {memory, Memory},
       {reductions, Reds},
       {first_seen, format_timestamp(T)}
      ]
      || {Pid, RegName, MsgQLen, Memory, Reds, T} <- Procs ].

format_memory(Mem) ->
    [ {Type, Size / 1048576} || {Type, Size} <- Mem ].
